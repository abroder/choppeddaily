# Installing on a Raspberry Pi

curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
sudo apt update && sudo apt install yarn libcairo2-dev libjpeg-dev libpango1.0-dev libgif-dev build-essential
yarn install

populate config.js

```
module.exports = {
  Twit: {
    consumer_key: '',
    consumer_secret: '',
    access_token: '',
    access_token_secret: '',
  },
  GSE: {
    cse_id: '',
    api_key: '',
  }
}
```

GSE - custom search engine config
Twit - twitter app config
