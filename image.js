var googleImages = require('google-images')
var GSE = require('./config.js').GSE
var fs = require('fs')
var request = require('request')
var Canvas = require('canvas')
var Image = Canvas.Image

var xStarts = {
  80: 273,
  100: 267,
  120: 257,
  140: 252,
  160: 249,
  180: 244,
  200: 237,
  220: 233,
  240: 231,
  260: 230,
  280: 230,
  300: 230,
  320: 230,
  340: 230,
  360: 230,
  380: 230,
  400: 230
}

var isTooWide = function (ctx, text, width) {
  if (typeof text === 'string') {
    return ctx.measureText(text).width > width 
  }

  for (var i = 0; i < text.length; i++) {
    if (ctx.measureText(text[i]).width > width) {
      return true
    }
  }

  return false
}

var lineHeight = function(ctx, string) {
  var measurement = ctx.measureText(string)
  return measurement.emHeightAscent + measurement.emHeightDescent
}

var textHeight = function (ctx, text) {
  if (typeof text === 'string') {
    return lineHeight(ctx, text)
  }

  var height = 0
  for (var i = 0; i < text.length; i++) {
    height += lineHeight(ctx, text[i])
  }

  return height
}

var splitToFit = function (ctx, string, width) {
  var lines = [string]
  var iterCount = 0

  while (isTooWide(ctx, lines, width)) {
    for (var i = 0; i < lines.length; i++) {
      var line = lines[i]
      if (isTooWide(ctx, line, width)) {
        var spaceIndex = line.lastIndexOf(' ')
        lines[i] = line.substr(0, spaceIndex)
        if (i !== lines.length - 1) {
          lines[i + 1] = line.substr(spaceIndex + 1) + ' ' + lines[i + 1]
        } else {
          lines.push(line.substr(spaceIndex + 1))
        }
      }
    }
    iterCount++
    if (iterCount > 10) break
  }

  return lines
}

var makeImage = function (ingredients) {
  var shownIngredient = ingredients[Math.floor(Math.random() * ingredients.length)]

  return new Promise(function (resolve, reject) {
    var client = googleImages(GSE.cse_id, GSE.api_key)

    client.search(shownIngredient, { size: 'large' }).
    then(function (images) {
      var filename = __dirname + '/tmp-' + shownIngredient.replace(/ /g, '-').toLowerCase()

      request(images[0].url).pipe(fs.createWriteStream(filename)).on('close', function () {
        var canvas = new Canvas(720, 405)
        var ctx = canvas.getContext('2d')

        var ingredientImg = new Image
        ingredientImg.onerror = function () {
          reject(arguments)
        }
        ingredientImg.onload = function () {
          var overlayImg = new Image
          overlayImg.src = fs.readFileSync(__dirname + '/ingredients.png')

          var width = overlayImg.width
          var height = ingredientImg.height * (overlayImg.width / ingredientImg.width)

          ctx.drawImage(ingredientImg, 200, (405 - height) / 2, width, height)
          ctx.drawImage(overlayImg, 0, 0, overlayImg.width, overlayImg.height)

          ctx.font = '20px Eurostile'
          ctx.fillStyle = '#fff'
	  ctx.textAlign = 'right'

          var textStrings = []

          for (var i = 0; i < ingredients.length; i++) {
	    var ingredient = ingredients[i].toUpperCase()

            var splitIngredient = splitToFit(ctx, ingredient, 230)
            textStrings = textStrings.concat(splitIngredient)
            if (i !== ingredients.length - 1) {
              textStrings.push('')
            }
          }

          var startY = 70 + (350 - textHeight(ctx, textStrings)) / 2

          for (var i = 0; i < textStrings.length; i++) {
            var y = startY + (20 * i)
            var x = xStarts[Math.floor(y / 20) * 20]
            ctx.fillText(textStrings[i], x, y)
          }

          fs.unlinkSync(filename)
          resolve(canvas)
        }
        ingredientImg.src = fs.readFileSync(filename) 
      }, function (reason) { reject(reason) })
    }, function (reason) { reject(reason) })
  })
}

module.exports = { makeImage }
