#!/usr/bin/env node

var fs = require('fs')
var Twit = require('twit')
var T = new Twit(require('./config.js').Twit)
var sleep = require('sleep').sleep
var async = require('async')
var makeImage = require('./image.js').makeImage

var ingredients = fs.readFileSync(__dirname + '/ingredients.txt').toString().split('\n')

var cloneObject = function (obj, propertyList) {
  var result = {}
  for (var i = 0; i < propertyList.length; i++) {
    var property = propertyList[i]
    if (obj[property]) {
      result[property] = obj[property]
    }
  }

  return result
}

var sendTweet = function (tweet, lastId) {
  if (typeof tweet === 'string') {
    var params = { status: tweet }
    if (lastId) {
      params.in_reply_to_status_id = lastId
    }

    return T.post('statuses/update', params)
  } else {
    if (tweet.wait) {
      sleep(tweet.wait)
    }

    var params = cloneObject(tweet, ['status'])
    if (lastId) {
      params.in_reply_to_status_id = lastId
    }

    if (tweet.media) {
      return T.post('media/upload', { media_data: tweet.media }).
        then(function (result) {
          params.media_ids = [result.data.media_id_string]
          return T.post('statuses/update', params)
        })
    } else {
      return T.post('statuses/update', params)
    }
  }
}

var sendTweetstorm = function (tweets) {
  return new Promise(function (resolve, reject) {
    // TODO: is there a better way to do this
    var lastId
    
    async.eachSeries(tweets, function (tweet, callback) {
      sendTweet(tweet, lastId).
      then(function (result) {
        lastId = result.data.id_str
        callback()
      }, function (err) {
        callback(err)
      })
    }, function (err) {
      if (err) {
        reject(err)
        return
      }
       
      resolve() 
    })
  })
}

var getIngredient = function () {
  return ingredients[Math.floor(Math.random() * ingredients.length)]
}

var getIngredients = function () {
  return [ getIngredient(), getIngredient(), getIngredient(), getIngredient() ] 
}

var getIngredientsString = function (ingredients) {
  var result = ''
  for (var i = 0; i < ingredients.length - 1; i++) {
    result += ingredients[i] + ', ';
  }
  result += 'and ' + ingredients[ingredients.length - 1]
  return result
}

var generateAppetizer = function () {
  var ingredients = getIngredients()
  var appetizer = `Make an appetizer that includes ${ getIngredientsString(ingredients) }.`
  return (appetizer.length <= 140) ? { basket: ingredients, tweet: appetizer }: generateAppetizer()
}

var generateEntree = function () {
  var ingredients = getIngredients()
  var entree = `And your entrée must include ${ getIngredientsString(ingredients) }.`
  return (entree.length <= 140) ? { basket: ingredients, tweet: entree } : generateEntree()
}

var generateDessert = function () {
  var ingredients = getIngredients()
  var dessert = `Make a dessert with ${ getIngredientsString(ingredients) }.`
  return (dessert.length <= 140) ? { basket: ingredients, tweet: dessert } : generateDessert()
}

var generateChopped = function () {
  var appetizer = generateAppetizer()
  var entree = generateEntree()
  var dessert = generateDessert()

  Promise.all([makeImage(appetizer.basket), makeImage(entree.basket), makeImage(dessert.basket)]).
  then(function (canvases) {
    var tweets = [
      'Welcome to the Chopped kitchen! ' +
        'The challenge: create an unforgettable meal from the mystery ' +
        'items hidden in our baskets.',
      'There are 3 rounds: appetizer, entrée, & dessert. ' +
        'Each round will have 4 mystery ingredients, and you must use all ' +
        'ingredients in some way.',
      {
        status: appetizer.tweet,
        media: canvases[0].toBuffer().toString('base64')
      },
      'You have 20 minutes starting… now!',
      {
        status: 'Time is up! Now, for the entrée round.',
        wait: 20 * 60
      },
      {
        status: entree.tweet,
        media: canvases[1].toBuffer().toString('base64')
      },
      'You have 30 minutes starting… now!',
      {
        status: 'Time is up! Please take a step back and prepare for the ' +
          'dessert round.',
        wait: 30 * 60
      },
      {
        status: dessert.tweet,
        media: canvases[2].toBuffer().toString('base64')
      },
      'You have 30 minutes starting… now!',
      {
        status: 'Time\'s up! That\'s all for tonight, join us tomorrow for ' +
          'a new episode of @choppeddaily.',
        wait: 30 * 60
      }
    ]

    return sendTweetstorm(tweets)
  }).
  then(function () {
  }, function (reason) {
    console.log(reason)
  })
}

generateChopped()
