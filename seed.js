var request = require('request')
var cheerio = require('cheerio')
var fs = require('fs')

var result = {}

request('https://en.wikipedia.org/wiki/List_of_Chopped_episodes', function (err, res, body) {
	var $ = cheerio.load(body);
	var rounds = $('td[colspan=3] li')
	rounds.each(function (i, round) {
    var ingredients = $(this).text().split(':')[1]
    if (ingredients.length == 0) return

    var list = ingredients.split(',')
    list = list.map((str) => { 
      return str.trim().toLowerCase()
    })
    for (var i = 0; i < list.length; i++) {
      result[list[i]] = true
    }
	})

  var stream = fs.createWriteStream('ingredients.txt')
  stream.once('open', function () {
    for (var ingredient in result) {
      if (!result.hasOwnProperty(ingredient)) return

      stream.write(ingredient + '\n')
    }
    stream.end()
  })
})
